//
//  ProductCollectionViewCell.m
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import "ProductCollectionViewCell.h"

@implementation ProductCollectionViewCell
#pragma mark - Accessors
- (UIImageView *)imageView {
    if (!self.productImageView) {
        self.productImageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        self.productImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.productImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return self.productImageView;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self.contentView addSubview:self.imageView];
    }
    return self;
}
@end
