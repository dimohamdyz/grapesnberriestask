//
//  NSDictionary+nullFreeDictionaryWithDictionary.h
//  Taree2y
//
//  Created by binaryboy on 8/25/14.
//  Copyright (c) 2014 Link Dev. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface NSDictionary (nullFreeDictionaryWithDictionary)
+ (NSDictionary *)nullFreeDictionaryWithDictionary:(NSDictionary *)dictionary;

@end
