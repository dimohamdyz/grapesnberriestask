//
//  PPCustomSegue.m
//  PopPlayground
//
//  Created by Erik Dungan on 5/2/14.
//  Copyright (c) 2014 Callmeed. All rights reserved.
//
//  Special thanks to Zakir Hyder for his blog post on custom segues
//  http://blog.jambura.com/2012/07/05/custom-segue-animation-left-to-right-using-catransition/


#import "PPCustomSegue.h"
#import <POP/POP.h>

@implementation PPCustomSegue

-(void)perform {
    
    UIViewController *sourceViewController = (UIViewController*)[self sourceViewController];
    UIViewController *destinationController = (UIViewController*)[self destinationViewController];
    
    CALayer *layer = destinationController.view.layer;
    [layer pop_removeAllAnimations];
    
    NSLog(@"Layer frame X: %f", layer.frame.origin.x);
    NSLog(@"Layer frame width: %f", layer.frame.size.width);
    
    
    //New Paper animation
    
    POPSpringAnimation *spin =
    [POPSpringAnimation animationWithPropertyNamed:kPOPLayerRotation];
    spin.toValue = @(M_PI*4);
    spin.springBounciness = 15;
    spin.springSpeed = 5.0f;
    [layer pop_addAnimation:spin forKey:@"spin"];
    
    [sourceViewController.navigationController pushViewController:destinationController animated:NO];

}

@end
