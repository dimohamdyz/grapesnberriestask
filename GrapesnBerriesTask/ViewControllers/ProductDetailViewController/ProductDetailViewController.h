//
//  ProductDetailViewController.h
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/6/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"
@interface ProductDetailViewController : UIViewController
@property (nonatomic,strong)ProductObject *product;
@property (strong, nonatomic) IBOutlet UIImageView *productImageView;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@end
