//
//  ProductDetailViewController.m
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/6/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import "ProductDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SLColorArt.h"
#import "UIImage+Scale.h"
#import "UIImage+ColorArt.h"
@interface ProductDetailViewController ()

@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.productImageView sd_setImageWithURL:[NSURL URLWithString:self.product.productImage.url] placeholderImage:[UIImage imageNamed:@"No_image_available"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self colorizeForImage:image];
        
    }];
    
    
    self.priceLabel.text = [NSString stringWithFormat:@"Price %d",self.product.price];
    self.productDescriptionLabel.text = [NSString stringWithFormat:@"Description %@",self.product.productDescription] ;
}
- (void)colorizeForImage:(UIImage *)image{
    
    SLColorArt *colorArt = [image colorArt];
    self.view.backgroundColor =  colorArt.backgroundColor;
    
    
    self.priceLabel.backgroundColor = colorArt.backgroundColor;
    self.productDescriptionLabel.backgroundColor = colorArt.backgroundColor;
    
    
    self.priceLabel.textColor = colorArt.primaryColor;
    self.productDescriptionLabel.textColor = colorArt.secondaryColor;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
