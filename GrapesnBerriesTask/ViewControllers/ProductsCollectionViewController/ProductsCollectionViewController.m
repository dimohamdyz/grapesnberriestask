//
//  ProductsCollectionViewController.m
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import "ProductsCollectionViewController.h"
#import "ProductCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProductObject.h"
#import "UIScrollView+InfiniteScroll.h"
#import "CustomInfiniteIndicator.h"
#import "ProductModel.h"



#import "SLColorArt.h"
#import "UIImage+Scale.h"
#import "UIImage+ColorArt.h"

#import "MBProgressHUD.h"

#import "ProductDetailViewController.h"
@interface ProductsCollectionViewController ()
@property (nonatomic, strong) NSMutableArray *cellSizes;
//@property (nonatomic, strong) NSMutableArray *indexPaths;

@property (nonatomic, strong) ProductObject *product;

@property (nonatomic, strong) NSMutableArray *products;


@end

@implementation ProductsCollectionViewController


static NSString * const reuseIdentifier = @"ProductCollectionViewCell";
#pragma mark - Accessors

- (void)colorizeForImage:(UIImage *)image forCell:(ProductCollectionViewCell*)cell{
    
    SLColorArt *colorArt = [image colorArt];
    cell.priceLabel.backgroundColor = colorArt.backgroundColor;
    cell.decriptionLabel.backgroundColor = colorArt.backgroundColor;
    cell.priceLabel.textColor = colorArt.primaryColor;
    cell.decriptionLabel.textColor = colorArt.secondaryColor;
}

- (NSArray *)cellSizes {

    return _cellSizes;
}
#pragma mark - Life Cycle

- (void)dealloc {
    self.collectionView.delegate = nil;
    self.collectionView.dataSource = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    CHTCollectionViewWaterfallLayout *layout = (CHTCollectionViewWaterfallLayout *)self.collectionView.collectionViewLayout;
    
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    
    layout.minimumColumnSpacing = 20;
    layout.minimumInteritemSpacing = 30;
    
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.cellSizes = [NSMutableArray new];
    
    
    
    __weak typeof(self) weakSelf = self;
    
    CGRect indicatorRect = CGRectMake(0, 0, 24, 24);
    
    CustomInfiniteIndicator *indicator = [[CustomInfiniteIndicator alloc] initWithFrame:indicatorRect];
    
    // Set custom indicator
    self.collectionView.infiniteScrollIndicatorView = indicator;
    
    // Set custom indicator margin
    self.collectionView.infiniteScrollIndicatorMargin = 40;
    
    // Add infinite scroll handler
    [self.collectionView addInfiniteScrollWithHandler:^(UICollectionView *collectionView) {
        
        [[ProductModel sharedInstance]getProductDataWithCompletion:^(NSMutableArray *products) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"inside dispatch async block main thread from main thread");
                
                //bug and answer http://stackoverflow.com/questions/32680303/ios9-this-application-is-modifying-the-autolayout-engine-from-a-background-thr
                
            NSInteger index = weakSelf.products.count;
            NSMutableArray *indexPaths = [NSMutableArray new];
            
            // create index paths for affected items
            for(ProductObject *product in products) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index++ inSection:0];
                
                [weakSelf.products addObject:product];
                [weakSelf.cellSizes addObject:[NSValue valueWithCGSize:CGSizeMake(product.productImage.width, product.productImage.height)]];
                
                [indexPaths addObject:indexPath];
            }
            
            // Update collection view
            [collectionView performBatchUpdates:^{
                // add new items into collection
                if (indexPaths != nil) {
                    [collectionView insertItemsAtIndexPaths:indexPaths];
                    
                }
                
            } completion:^(BOOL finished) {
                // finish infinite scroll animations
                [collectionView finishInfiniteScroll];
            }];
            
              });
        }];
        
        
    }];
    
    
    // Load initial data
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:NO];
    
    [[ProductModel sharedInstance]getProductDataWithCompletion:^(NSMutableArray *products) {
        self.products = products;
        
        // create index paths for affected items
        for(ProductObject *product in products) {
            [self.cellSizes addObject:[NSValue valueWithCGSize:CGSizeMake(product.productImage.width, product.productImage.height)]];
        }
        self.collectionView.dataSource = self;
        self.collectionView.delegate = self;
        [self.collectionView reloadData];
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:NO];
        
    }];


}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateLayoutForOrientation:[UIApplication sharedApplication].statusBarOrientation];

#if DEBUG
    //Debug the Load Font
    for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
#endif
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"QuicksandLight-Regular" size:21]}];
    self.navigationController.navigationBar.topItem.title = @"Products";
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateLayoutForOrientation:toInterfaceOrientation];
}

- (void)updateLayoutForOrientation:(UIInterfaceOrientation)orientation {
    CHTCollectionViewWaterfallLayout *layout =
    (CHTCollectionViewWaterfallLayout *)self.collectionView.collectionViewLayout;
    layout.columnCount =  UIInterfaceOrientationIsPortrait(orientation) ? 2 : 3;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.products.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    NSLog(@"Pretty printed size: %@", NSStringFromCGSize(cell.frame.size));

    
    ProductObject *product = [self.products objectAtIndex:indexPath.row];
    
      [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:product.productImage.url] placeholderImage:[UIImage imageNamed:@"No_image_available"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
          if (error == nil) {
              [self colorizeForImage:image forCell:cell];
          }

      }];
    

    cell.priceLabel.text = [NSString stringWithFormat:@"%d",product.price];
    cell.decriptionLabel.text = product.productDescription;
    return cell;
}


#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self.cellSizes[indexPath.item] CGSizeValue];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.product = [self.products objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"productDetails" sender:self];
}

 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     ProductDetailViewController *productDetailViewController  =(ProductDetailViewController*)[segue destinationViewController];
     productDetailViewController.product = self.product;
     
 }
 

@end
