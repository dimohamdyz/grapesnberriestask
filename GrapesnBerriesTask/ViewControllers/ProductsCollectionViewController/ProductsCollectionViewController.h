//
//  ProductsCollectionViewController.h
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHTCollectionViewWaterfallLayout.h"

@interface ProductsCollectionViewController : UICollectionViewController<CHTCollectionViewDelegateWaterfallLayout>

@end
