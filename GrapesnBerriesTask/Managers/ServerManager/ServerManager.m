//
//  ServerManager.m
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import "ServerManager.h"
#import "ProductObject.h"
#import "UIApplication+NetworkIndicator.h"

@implementation ServerManager

#define API_URL @"http://grapesnberries.getsandbox.com/products?"

- (void)getProductsFromCurrentIndex:(int)currentIndex andUsingLimit:(int)limit completion:(void (^)(BOOL finished,NSMutableArray *products))completion{
    [[UIApplication sharedApplication] startNetworkActivity];

    NSString *urlString = [NSString stringWithFormat:@"%@count=%d&from=%d",API_URL,limit,currentIndex];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.timeoutIntervalForRequest = 30.0;
    config.timeoutIntervalForResource = 60.0;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration: config delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];

        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
            // Handle response here
            
            NSMutableArray *productArray = [[NSMutableArray alloc] init];
            NSArray *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                if (data != nil) {
                        
                        for (NSDictionary *dic in responseDic) {
                            [productArray addObject:[[ProductObject alloc]initWithDictionaryOfData:dic]];
                        }
                        [[UIApplication sharedApplication] stopNetworkActivity];

                        completion(YES,productArray);
                        
                    
                }
        }];
        
        [postDataTask resume];
    
}
@end
