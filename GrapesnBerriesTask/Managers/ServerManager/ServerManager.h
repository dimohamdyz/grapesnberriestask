//
//  ServerManager.h
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServerManager : NSObject<NSURLSessionDataDelegate>
- (void)getProductsFromCurrentIndex:(int)currentIndex andUsingLimit:(int)limit completion:(void (^)(BOOL finished,NSMutableArray *products))completion;
@end
