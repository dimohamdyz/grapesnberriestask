//
//  DatabaseManager.m
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//


#import "DatabaseManager.h"
#import <sqlite3.h>
#import "FMDatabase.h"
@interface DatabaseManager()
@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;
@property (nonatomic, strong) FMDatabase *db;
@end

@implementation DatabaseManager
- (instancetype)init{
    self = [super init];
    if (self) {
        // Set the documents directory path to the documentsDirectory property.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        
        // Keep the database filename.
        self.databaseFilename = @"grapesnberries.sqlite";
        
        // Copy the database file into the documents directory if necessary.
        [self copyDatabaseIntoDocumentsDirectory];
        
        
    }
    return self;
}
/**
 * Get the path of .sqlite database
 *
 *  @return Database path
 */
- (NSString*)getDatabasePath{
    return [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
}

/**
 *  Copy database into documents directory
 */
- (void)copyDatabaseIntoDocumentsDirectory{
    // Check if the database file exists in the documents directory.
    NSString *destinationPath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
        //NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:self.databaseFilename ofType:nil];
//        NSLog(@"#### %@", sourcePath);
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        
        // Check if any error occurred during copying and display it.
        if (error != nil) {
            NSLog(@"gives error: %@", [error localizedDescription]);
        }
        else
        {
            NSLog(@"copydone IN %@",destinationPath);
            self.db = [FMDatabase databaseWithPath:destinationPath];
            [self.db open];
        }
    }
    else
    {
        NSLog(@"database exists IN %@",destinationPath);
        self.db = [FMDatabase databaseWithPath:destinationPath];
        [self.db open];
    }
}

#pragma mark - Projects

- (NSMutableArray *)getProductFromIndex:(int)currentIndex withLimitNumber:(int)limit{
    if (![self.db open]) {
        return nil;
    }else{
        NSMutableArray *arrayOfDictionary = [[NSMutableArray alloc] init];;
        
        FMResultSet *results = [self.db executeQuery:@"SELECT * FROM Product WHERE ID >= ? AND ID <= ?",[NSNumber numberWithInt:currentIndex ],[NSNumber numberWithInt:currentIndex+limit]];
        while([results next])
        {
            [arrayOfDictionary addObject:[results resultDictionary]];
            
        }
        return arrayOfDictionary;
    }
}
- (void)insertProject:(ProductObject*)product{
    
    if (![self.db open]) {
        return ;
    }else{

        NSString *sqlSL = @"INSERT OR REPLACE INTO Product (ID, productDescription, price , imageWidth , imageHeight,imageUrl) VALUES (?, ? ,? , ?,?,?)";
        [self.db beginTransaction];
        [self.db executeUpdate:sqlSL,[NSNumber numberWithInt:product.ID] , product.productDescription, [NSNumber numberWithInt:product.price],[NSNumber numberWithInt:product.productImage.width],[NSNumber numberWithInt:product.productImage.height],product.productImage.url];
        [self.db commit];
    }
}
-(int)getIndexNumber{
    
    if (![self.db open]) {
        return 0;
    }else{
        
        FMResultSet *results = [self.db executeQuery:@"SELECT COUNT(*)  as count FROM Product"];
        [results next];
        return [results intForColumn:@"count"];
    }
}
#pragma mark - Open Data Base Methods
- (void)openDataBase{
    [self.db open];
}

#pragma mark - Close Data Base Methods
- (void)closeDataBase{
    [self.db close];
}
@end
