//
//  DatabaseManager.h
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ProductObject.h"

@interface DatabaseManager : NSObject

- (instancetype)init;


- (NSMutableArray *)getProductFromIndex:(int)currentIndex withLimitNumber:(int)limit;
- (void)insertProject:(ProductObject*)product;
-(int)getIndexNumber;
#pragma mark - Open Data Base Methods
- (void)openDataBase;
#pragma mark - Close Data Base Methods
- (void)closeDataBase;
@end
