//
//  ProductImageObject.m
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import "ProductImageObject.h"
#import "NSDictionary+nullFreeDictionaryWithDictionary.h"

@implementation ProductImageObject
/**
 *  It parses dictionary to current object
 *
 *  @param dic NSDictionary from webservices/database
 *
 *  @return ProductImageObject object
 */
- (id)initWithDictionaryOfData:(NSDictionary*)dic{
    self = [super init];
    if(self){
        
        dic=[NSDictionary nullFreeDictionaryWithDictionary:dic];
        
        self.width = [[dic valueForKey:@"width"] intValue];
        self.height = [[dic valueForKey:@"height"] intValue];
        self.url = [dic valueForKey:@"url"];
        
    }
    return self;
    
}
@end
