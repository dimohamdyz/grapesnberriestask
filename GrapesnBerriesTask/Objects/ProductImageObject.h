//
//  ProductImageObject.h
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductImageObject : NSObject
@property (nonatomic) int width;
@property (nonatomic)int  height;
@property (nonatomic, copy) NSString  *url;
- (id)initWithDictionaryOfData:(NSDictionary*)dic;

@end
