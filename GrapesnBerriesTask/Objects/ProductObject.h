//
//  ProductObject.h
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductImageObject.h"
@interface ProductObject : NSObject
@property (nonatomic) int ID;
@property (nonatomic, copy) NSString *productDescription;
@property (nonatomic) int price;
@property (nonatomic, strong) ProductImageObject *productImage;

- (id)initWithDictionaryOfData:(NSDictionary*)dic;

@end