//
//  ProductObject.m
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import "ProductObject.h"
#import "NSDictionary+nullFreeDictionaryWithDictionary.h"

@implementation ProductObject
/**
 *  It parses dictionary to current object
 *
 *  @param dic NSDictionary from webservices/database
 *
 *  @return ProductObject object
 */
- (id)initWithDictionaryOfData:(NSDictionary*)dic{
    self = [super init];
    if(self){
        
        dic = [NSDictionary nullFreeDictionaryWithDictionary:dic];

        if ([dic valueForKey:@"id"] == nil) {
            self.ID = [[dic valueForKey:@"ID"]intValue];

        }else{
            self.ID = [[dic valueForKey:@"id"]intValue];

        }

        self.productDescription = [dic valueForKey:@"productDescription"];
        self.price = [[dic valueForKey:@"price"]intValue];
        if ([dic valueForKey:@"image"] == nil) {
            
            NSDictionary *imageDic = [NSDictionary dictionaryWithObjectsAndKeys:[dic valueForKey:@"imageWidth"],@"width",[dic valueForKey:@"imageHeight"],@"height",[dic valueForKey:@"imageUrl"],@"url",nil];
   
            self.productImage = [[ProductImageObject alloc] initWithDictionaryOfData:imageDic];

        }else{
            self.productImage = [[ProductImageObject alloc] initWithDictionaryOfData:[dic valueForKey:@"image"]];
   
        }

    }
    return self;
    
}
@end
