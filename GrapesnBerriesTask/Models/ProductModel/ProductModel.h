//
//  ProductModel.h
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductModel : NSObject
+ (ProductModel*)sharedInstance;
-(void)getProductDataWithCompletion:(void (^)(NSMutableArray *products))completion;
@end
