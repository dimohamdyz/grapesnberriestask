//
//  ProductModel.m
//  GrapesnBerriesTask
//
//  Created by binaryboy on 11/4/15.
//  Copyright © 2015 AhmedHamdy. All rights reserved.
//

#import "ProductModel.h"
#import "Reachability.h"
#import "ServerManager.h"
#import "DatabaseManager.h"
@implementation ProductModel
static Reachability * reachability;
static int currentIndex;
static int lastIndex = 0;

#define limit 10

+ (ProductModel *) sharedInstance
{
    static dispatch_once_t onceToken;
    static ProductModel *singelton = nil;
    dispatch_once(&onceToken, ^{
        singelton = [[ProductModel alloc] init];
    });
    return singelton;
}

-(void)getProductDataWithCompletion:(void (^)(NSMutableArray *products))completion{
    DatabaseManager *dataBaseManager = [[DatabaseManager alloc]init];

    lastIndex = [dataBaseManager getIndexNumber];
    if (lastIndex > currentIndex) {
        //load direct from DataBase
        NSLog(@"Get From DataBase");
        
        DatabaseManager *dataBaseManager = [[DatabaseManager alloc]init];
        NSMutableArray *dictionaryData = [dataBaseManager getProductFromIndex:currentIndex withLimitNumber:currentIndex +limit];
        NSMutableArray *productsArray = [NSMutableArray new];
        for (NSDictionary *dic in dictionaryData) {
            [productsArray addObject:[[ProductObject alloc]initWithDictionaryOfData:dic]];
        }
        currentIndex+=limit;

        completion(productsArray);
    }else{
        //get More data From API
        
        reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
        [reachability startNotifier];
        
        //Have Internet Connection
        reachability.reachableBlock = ^(Reachability * reachability)
        {
            NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Reachable(%@)", reachability.currentReachabilityString];
            NSLog(@"%@", temp);
            
            ServerManager *serverManager = [[ServerManager alloc]init];
            [serverManager getProductsFromCurrentIndex:currentIndex andUsingLimit:limit completion:^(BOOL finished, NSMutableArray *products) {
                
                DatabaseManager *dataBaseManager = [[DatabaseManager alloc]init];
                
                for (ProductObject *product in products) {
                    [dataBaseManager insertProject:product];
                    
                }
                currentIndex+=limit;
                
                completion(products);
                
            }];
        };

    }
    
}
@end
